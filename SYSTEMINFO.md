# GitHub System

Vendor: GitHub
Homepage: https://github.com/

Product: GitHub System
Product Page: https://github.com/

## Introduction
We classify GitHub into the CI/CD domain as GitHub handles the versioning, building, storage and deploying of the system and components.

## Why Integrate
The GitHub adapter from Itential is used to integrate the Itential Automation Platform (IAP) with GitHub. With this adapter you have the ability to perform operations such as:

- Get Projects
- Get Repositories
- Get Repository
- Commit
- Create Pull Request
- Post Change

## Additional Product Documentation
The [API documents for GitHub](https://docs.github.com/en/rest?apiVersion=2022-11-28)