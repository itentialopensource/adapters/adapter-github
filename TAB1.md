# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the GitHub System. The API that was used to build the adapter for GitHub is usually available in the report directory of this adapter. The adapter utilizes the GitHub API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The GitHub adapter from Itential is used to integrate the Itential Automation Platform (IAP) with GitHub. With this adapter you have the ability to perform operations such as:

- Get Projects
- Get Repositories
- Get Repository
- Commit
- Create Pull Request
- Post Change

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
