
## 0.3.3 [06-07-2023]

* Patch/adapt 2390

See merge request itentialopensource/adapters/devops-netops/adapter-github!10

---

## 0.3.2 [04-06-2023]

* updated adapter utils ver

See merge request itentialopensource/adapters/devops-netops/adapter-github!11

---

## 0.3.1 [03-24-2023]

* updated adapter utils ver

See merge request itentialopensource/adapters/devops-netops/adapter-github!11

---

## 0.3.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/devops-netops/adapter-github!9

---

## 0.2.6 [08-02-2021]

- Change the healthcheck to something returning less data

See merge request itentialopensource/adapters/devops-netops/adapter-github!8

---

## 0.2.5 [03-22-2021]

- Add new call to adapter

See merge request itentialopensource/adapters/devops-netops/adapter-github!6

---

## 0.2.4 [03-17-2021]

- Add new required header to sampleProperties and need to encode the content for putReposOwnerRepoContentsPath

See merge request itentialopensource/adapters/devops-netops/adapter-github!5

---

## 0.2.3 [03-07-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/devops-netops/adapter-github!4

---

## 0.2.2 [07-08-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/devops-netops/adapter-github!3

---

## 0.2.1 [01-09-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/devops-netops/adapter-github!2

---

## 0.2.0 [11-07-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/devops-netops/adapter-github!1

---

## 0.1.1 [09-20-2019]

- Initial Commit

See commit cb9b579

---
